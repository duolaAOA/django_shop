# django_shop
python3.6

django1.11.7

django-rest-framework  WEB API

### [个人博客](http://140.143.18.253/)


### 认证模式
* 支持 `session` 与 `token` 认证

### 项目接口

API文档

* http://140.143.18.253:8000/docs/    

商品数据公共接口

* http://140.143.18.253:8000/goods/

商品分类列表数据接口

* http://140.143.18.253:8000/categorys/

验证码接口

* http://140.143.18.253:8000/codes/

用户列表接口

* http://140.143.18.253:8000/users/

用户收藏

* http://140.143.18.253:8000/userfavs/

留言

* http://140.143.18.253:8000/messages/

收货地址

* http://140.143.18.253:8000/address/

购物车

* http://140.143.18.253:8000/shopcarts/

订单信息

* http://140.143.18.253:8000/orders/